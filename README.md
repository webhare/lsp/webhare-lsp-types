# WebHare LSP types

This project exists to share LSP protocol types between the server in the [dev module](https://gitlab.com/webhare/dev) and clients such as
- [WebHare VS Code extension](https://gitlab.com/webhare/lsp/webhare-language-vscode)

## Updating
Before committing run `npm run test`. Make sure your changes are commited and then run `npm publish`
