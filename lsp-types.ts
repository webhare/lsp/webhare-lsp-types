/* Definitions for our custom language server requests, shared as type definitions to external users */

import { TextDocumentIdentifier } from "vscode-languageserver";

// getStackTrace response
export interface StackTraceResponse {
  errors: StackTraceError[];
}

// One stack trace entry
export interface StackTraceError {
  date: Date;
  guid: string;
  groupid: string;
  stack: StackTraceItem[];
}

// Stack trace item (the first item of a trace contains the error message,
// subsequent items contain calling functions)
interface StackTraceItem {
  message?: string;
  func?: string;
  filename: string;
  line: number;
  col: number;
  editorpath: string;
  primary: boolean;
}

// Parameters for the getStackTrace call
export interface StackTraceParams {
  textDocument: TextDocumentIdentifier;
  lastGuid?: string;
}


// getConfig response
export interface ConfigResponse {
  basedataroot: string;
  baseport: number;
  ephemeralroot: string;
  hstrustedport: number;
  installationroot: string;
  logroot: string;
  varroot: string;
  moduledirs: string[];
  trustedhost: string;
  trustedport: number;
}

// Parameters for the getConfig call
export interface ConfigParams {
  textDocument: TextDocumentIdentifier;
}
