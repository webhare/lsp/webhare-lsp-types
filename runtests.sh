#!/bin/bash
set -eo pipefail
cd "${BASH_SOURCE%/*}"
npm install
node_modules/.bin/tsc
echo "No issues!"
